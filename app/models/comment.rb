class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :post_id, message: 'Sorry, orphan comment isnt allowed in this app :P'
	validates_numericality_of :post_id, message: 'This should be number'
	validates_presence_of :body
end
